package com.hbctcf.news.international.main;

import com.hbctcf.news.international.pipeline.InternationalNewsPipeline;
import com.hbctcf.news.international.processor.InternationalNewsProcessor;
import com.hbctcf.news.international.utils.Constants;

import us.codecraft.webmagic.Spider;

public class InternationalNewsMain {

	public static void main(String[] args) {
		
		Spider.create(new InternationalNewsProcessor()).addUrl(Constants.INTERNATIONAL_TARGET_URL).
		addPipeline(new InternationalNewsPipeline()).start();

	}

}

package com.hbctcf.news.international.main;

import com.hbctcf.news.international.downloader.LagouAction;
import com.hbctcf.news.international.downloader.SeleniumDownloader;
import com.hbctcf.news.international.pipeline.LagouPipeline;
import com.hbctcf.news.international.processor.LagouProcessor;

import us.codecraft.webmagic.Spider;

public class LagouMain {

	public static void main(String[] args) {
		SeleniumDownloader downloader = new SeleniumDownloader();
		downloader.setOperator(new LagouAction());
		Spider.create(new LagouProcessor()).setDownloader(downloader).addUrl("https://www.lagou.com/jobs/mList.html").addPipeline(new LagouPipeline()).start();

	}

}

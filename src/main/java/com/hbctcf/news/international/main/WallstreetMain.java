package com.hbctcf.news.international.main;

import com.hbctcf.news.international.downloader.SeleniumDownloader;
import com.hbctcf.news.international.pipeline.WalltstreetcnPipeline;
import com.hbctcf.news.international.processor.WallstreetcnProcessor;
import com.hbctcf.news.international.utils.Constants;

import us.codecraft.webmagic.Spider;

public class WallstreetMain {

	public static void main(String[] args) {
		Spider.create(new WallstreetcnProcessor()).setDownloader(new SeleniumDownloader()).addUrl(Constants.WALLSTREET_TARGET_URL).addPipeline(new WalltstreetcnPipeline()).start();
	}

}

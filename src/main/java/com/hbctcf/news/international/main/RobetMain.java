package com.hbctcf.news.international.main;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.collect.Lists;

public class RobetMain {

	private static ChromeDriverService service;
	
	public static RemoteWebDriver getChromeDriver() throws IOException {
        System.setProperty("webdriver.chrome.driver","C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
        // 创建一个 ChromeDriver 的接口，用于连接 Chrome（chromedriver.exe 的路径可以任意放置，只要在newFile（）的时候写入你放的路径即可）
        service = new ChromeDriverService.Builder().usingDriverExecutable(new File("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe")) .usingAnyFreePort().build();
        service.start();
        // 创建一个 Chrome 的浏览器实例
        return new RemoteWebDriver(service.getUrl(), DesiredCapabilities.chrome());
    }
	
	
	//@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		try {
			RemoteWebDriver driver = getChromeDriver();
			 // 让浏览器访问 Baidu
			driver.get("http://127.0.0.1:8080/rxlcrm/");
	        System.out.println(" Page title is: " +driver.getTitle());
	     // 通过 id 找到 input 的 DOM
	        //WebElement usernameElement =driver.findElement(By.id("username"));
	        // 输入关键字
	        driver.findElement(By.id("username")).clear();driver.findElement(By.id("username")).sendKeys("admin");
	        driver.findElement(By.id("password")).clear();driver.findElement(By.id("password")).sendKeys("admin");
	        driver.findElementByClassName("btn-primary").click();
	        // 提交 input 所在的 form
	        // 通过判断 title 内容等待搜索页面加载完毕，间隔秒
	        new WebDriverWait(driver, 10).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					String title = input.getTitle();
	            	return ("任信了 快速开发平台".equals(title));
				}
			});
	        //driver.findElementByXPath("//a[contains(@data-href,'/rxlcrm/elastic/sys/menu/tree?parentId=31591bddeadc4afb87c0b02b7fb929dc')]").click();
	        //Thread.sleep(10000L);
	        // 显示搜索结果页面的 title
	        System.out.println(" Page title is: " +driver.getTitle());
	        driver.switchTo().frame("mainFrame");
	        WebElement er = driver.findElementByClassName("select2-focusser");
	        System.out.println(er.getAttribute("class"));
	        Select select = new Select(driver.findElementsByClassName("select2-offscreen").get(1));
	        select.selectByValue("3");
	        driver.findElementByXPath("//a[contains(@href,'/elastic/elastic/addElasticIndex')]").click();
	        Thread.sleep(2000L);
	        createIndex(driver);
	        
	        // 关闭浏览器
	        //driver.quit();
	        // 关闭 ChromeDriver 接口
	        //service.stop();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	private static void createIndex(RemoteWebDriver driver) throws InterruptedException {
		
		List<String> indexList = Lists.newArrayList();
		
		for(String index : indexList) {
			
			
			Select select = new Select(driver.findElementByName("businessType"));
	        select.selectByValue("3");
			String[] indexs = index.split(",");
			String enName = indexs[0];
			String chName = indexs[1];
			driver.findElementByName("idxNameZh").sendKeys(chName);
			driver.findElementByName("idxNameEn").sendKeys(enName);
			
			driver.findElementsByName("fieldIndex_1").get(0).click();
			driver.findElementsByName("fieldStore_1").get(0).click();
			driver.findElementsByName("fieldKeywd_1").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_2").sendKeys("唯一主键ID");
			driver.findElementByName("fieldEn_2").sendKeys("infono");
			driver.findElementsByName("fieldIndex_2").get(0).click();
			driver.findElementsByName("fieldKeywd_2").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_3").sendKeys("发送批次号");
			driver.findElementByName("fieldEn_3").sendKeys("msgid");
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_4").sendKeys("用户id");
			driver.findElementByName("fieldEn_4").sendKeys("userid");
			driver.findElementsByName("fieldIndex_4").get(0).click();
			driver.findElementsByName("fieldKeywd_4").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_5").sendKeys("通道id");
			driver.findElementByName("fieldEn_5").sendKeys("channelid");
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_6").sendKeys("提交时间");
			driver.findElementByName("fieldEn_6").sendKeys("submittime");
			driver.findElementsByName("fieldStore_6").get(0).click();
			driver.findElementsByName("fieldType_6").get(1).click();
			driver.findElementsByName("fieldIndex_6").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_7").sendKeys("手机号码");
			driver.findElementByName("fieldEn_7").sendKeys("phone");
			driver.findElementsByName("fieldStore_7").get(0).click();
			driver.findElementsByName("fieldIndex_7").get(0).click();
			driver.findElementsByName("fieldKeywd_7").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_8").sendKeys("短信内容");
			driver.findElementByName("fieldEn_8").sendKeys("msgcontent");
			driver.findElementsByName("fieldIndex_8").get(0).click();
			driver.findElementsByName("fieldKeywd_8").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_9").sendKeys("返回结果");
			driver.findElementByName("fieldEn_9").sendKeys("sumbitresult");
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_10").sendKeys("签名id");
			driver.findElementByName("fieldEn_10").sendKeys("signid");
			driver.findElementsByName("fieldIndex_10").get(0).click();
			driver.findElementsByName("fieldKeywd_10").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_11").sendKeys("类型");
			driver.findElementByName("fieldEn_11").sendKeys("protocoltype");
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_12").sendKeys("发送端口");
			driver.findElementByName("fieldEn_12").sendKeys("srcid");
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_13").sendKeys("总条数");
			driver.findElementByName("fieldEn_13").sendKeys("total");
			driver.findElementsByName("fieldType_13").get(2).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_14").sendKeys("状态");
			driver.findElementByName("fieldEn_14").sendKeys("stat");
			driver.findElementsByName("fieldIndex_14").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_15").sendKeys("状态报告时间");
			driver.findElementByName("fieldEn_15").sendKeys("reporttime");
			driver.findElementsByName("fieldType_15").get(1).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_16").sendKeys("类型名称");
			driver.findElementByName("fieldEn_16").sendKeys("categoryName");
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_17").sendKeys("父类类型名称");
			driver.findElementByName("fieldEn_17").sendKeys("categoryParentName");
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_18").sendKeys("类型ID");
			driver.findElementByName("fieldEn_18").sendKeys("categoryid");
			driver.findElementsByName("fieldType_18").get(2).click();
			driver.findElementsByName("fieldStore_18").get(0).click();
			driver.findElementsByName("fieldIndex_18").get(0).click();
			//停顿2秒
			driver.findElementById("fieldBtn").click();
			Thread.sleep(2000);
			
			driver.findElementByName("fieldZh_19").sendKeys("父类类型ID");
			driver.findElementByName("fieldEn_19").sendKeys("categoryPrentid");
			driver.findElementsByName("fieldStore_19").get(0).click();
			driver.findElementsByName("fieldType_19").get(2).click();
			driver.findElementsByName("fieldIndex_19").get(0).click();
			//停顿2秒
			driver.findElementById("submtBtn").click();
			Thread.sleep(16000);
			driver.findElementByXPath("//a[contains(@href,'/elastic/elastic/addElasticIndex')]").click();
	        Thread.sleep(2000L);
		}
		
	}

}

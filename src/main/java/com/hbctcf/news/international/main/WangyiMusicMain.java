package com.hbctcf.news.international.main;

import com.hbctcf.news.international.downloader.SeleniumDownloader;
import com.hbctcf.news.international.downloader.WangyiMusiceAction;
import com.hbctcf.news.international.dto.WangyiMusic;
import com.hbctcf.news.international.pipeline.WangyiMusicPipeline;

import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.model.OOSpider;

public class WangyiMusicMain {

	public static void main(String[] args) {
		SeleniumDownloader seleniumDownloader = new SeleniumDownloader();
		seleniumDownloader.setOperator(new WangyiMusiceAction());
		OOSpider.create(Site.me().setRetryTimes(3).setTimeOut(10000), new WangyiMusicPipeline(), WangyiMusic.class)
		.setDownloader(seleniumDownloader).addUrl("http://music.163.com/discover/album/#/?index=1").thread(5).runAsync();
		
		//Spider.create(new WallstreetcnProcessor()).setDownloader(new SeleniumDownloader()).addUrl("http://music.163.com/#/album?id=36017095").addPipeline(new WalltstreetcnPipeline()).start();
	}

}

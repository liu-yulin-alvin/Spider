package com.hbctcf.news.international.main;

import java.util.HashMap;
import java.util.Map;

import com.hbctcf.news.international.pipeline.HbctcfPipeline;
import com.hbctcf.news.international.processor.HbctcfProcessor;
import com.hbctcf.news.international.utils.Constants;

import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.HttpRequestBody;
import us.codecraft.webmagic.utils.HttpConstant;

public class HbctcfMain {
	
	private static final Request[] requests = new Request[6];

	static {
		for(int i = 0;i<6;i++){
			Map<String, Object> paramMap = new HashMap<String,Object>();
			paramMap.put("borrowStatus", 5);
			paramMap.put("pageNo",i+1);
			paramMap.put("pageSize", 7);
			Request request = new Request(Constants.HBCTCF_TARGET_URL);
			request.setMethod(HttpConstant.Method.POST);
			request.setRequestBody(HttpRequestBody.form(paramMap, "UTF-8"));
			requests[i] = request;
		}
	}
	
	public static void main(String[] args) {
		Spider.create(new HbctcfProcessor()).addRequest(requests).addPipeline(new HbctcfPipeline()).start();

	}

}

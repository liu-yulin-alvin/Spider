package com.hbctcf.news.international.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Hbctcf {
	
	private String borrowTitle;
	
	private String borrowSum;
	
	private String borrowRate;
	
	private String borrowTerm;
	
	private String borrowMinInvestMoney;

	public String getBorrowTitle() {
		return borrowTitle;
	}

	public void setBorrowTitle(String borrowTitle) {
		this.borrowTitle = borrowTitle;
	}

	public String getBorrowSum() {
		return borrowSum;
	}

	public void setBorrowSum(String borrowSum) {
		this.borrowSum = borrowSum;
	}

	public String getBorrowRate() {
		return borrowRate;
	}

	public void setBorrowRate(String borrowRate) {
		this.borrowRate = borrowRate;
	}

	public String getBorrowTerm() {
		return borrowTerm;
	}

	public void setBorrowTerm(String borrowTerm) {
		this.borrowTerm = borrowTerm;
	}

	public String getBorrowMinInvestMoney() {
		return borrowMinInvestMoney;
	}

	public void setBorrowMinInvestMoney(String borrowMinInvestMoney) {
		this.borrowMinInvestMoney = borrowMinInvestMoney;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}

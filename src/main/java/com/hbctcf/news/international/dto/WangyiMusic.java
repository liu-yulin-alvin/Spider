package com.hbctcf.news.international.dto;

import java.util.Date;
import java.util.List;

import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.Formatter;
import us.codecraft.webmagic.model.annotation.HelpUrl;
import us.codecraft.webmagic.model.annotation.TargetUrl;
@HelpUrl("http://music.163.com/discover/album/#/\\?index=\\d+")
@TargetUrl("http://music.163.com/album\\?id=\\d+")
public class WangyiMusic {

	@ExtractBy("//div[@class='g-wrap6']/div[@class='m-info f-cb']/div[@class='cnt']/div[@class='cntc']/div[@class='topblk']/div[@class='hd f-cb']/div[@class='tit']/h2[@class='f-ff2']/text()")
	private String title;
	@ExtractBy("//div[@class='g-wrap6']/div[@class='m-info f-cb']/div[@class='cnt']/div[@class='cntc']/div[@class='topblk']/div[@class='hd f-cb']/div[@class='tit']/div[@class='subtit f-fs1 f-ff2']/text()")
	private String remark;
	@ExtractBy("//div[@class='g-wrap6']/div[@class='m-info f-cb']/div[@class='cnt']/div[@class='cntc']/div[@class='topblk']/p[@class='intr'][1]/span/a[@class='s-fc7']/text()")
	private String author;
	@Formatter("yyyy-MM-dd")
	@ExtractBy("//div[@class='g-wrap6']/div[@class='m-info f-cb']/div[@class='cnt']/div[@class='cntc']/div[@class='topblk']/p[@class='intr'][2]/text(2)")
	private Date publishDate;
	@ExtractBy("//div[@class='g-wrap6']/div[@class='m-info f-cb']/div[@class='cnt']/div[@class='cntc']/div[@class='topblk']/p[@class='intr'][3]/text(2)")
	private String company;
	@ExtractBy("//div[@class='g-wrap6']/div[@class='n-albdesc']/p[@class='f-brk']/text()")
	private String description;
	@ExtractBy("//div[@class='j-flag']/table[@class='m-table']/tbody/tr[@class='even']/td/div[@class='f-cb']/div[@class='tt']/div[@class='ttc']/span[@class='txt']/a/b/text()")
	private List<String> songNameList;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getSongNameList() {
		return songNameList;
	}
	public void setSongNameList(List<String> songNameList) {
		this.songNameList = songNameList;
	}
	
}

package com.hbctcf.news.international.dto;

import java.util.Date;
import java.util.List;

public class Music163 {

	private String title;
	private String remark;
	private String author;
	private Date publishDate;
	private String company;
	private String description;
	private List<String> songNameList;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getSongNameList() {
		return songNameList;
	}
	public void setSongNameList(List<String> songNameList) {
		this.songNameList = songNameList;
	}
}

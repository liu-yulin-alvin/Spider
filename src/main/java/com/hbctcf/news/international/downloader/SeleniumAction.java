package com.hbctcf.news.international.downloader;

import org.openqa.selenium.WebDriver;

public interface SeleniumAction {
	 void execute(WebDriver driver);
}

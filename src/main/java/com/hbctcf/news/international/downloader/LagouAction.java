package com.hbctcf.news.international.downloader;

import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import com.hbctcf.news.international.utils.PropertiesUtils;

public class LagouAction implements SeleniumAction {

	@Override
	public void execute(WebDriver driver) {
		try {
			//driver.get("https://passport.lagou.com/login/login.html?service=https%3a%2f%2fwww.lagou.com%2f");
			driver.get("https://www.lagou.com/jobs/mList.html");
			if(driver.getCurrentUrl().split("?")[0].equals("https://www.lagou.com/jobs/mList.html")){
				return;
			}
 			//driver.findElement(By.className("content_box cleafix").className("left_area fl").className("form_body").);
			String baseBy = "//section[@class='content_box cleafix']/div[@class='left_area fl']/div[@class='form_body']/form";
			driver.findElement(By.xpath(baseBy+"/div[@class='input_item clearfix']/input[@type='text']")).clear();
			driver.findElement(By.xpath(baseBy+"/div[@class='input_item clearfix']/input[@type='text']")).sendKeys(PropertiesUtils.getConfig("lagou.username"));

			Thread.sleep(5000);

			driver.findElement(By.xpath(baseBy+"/div[@class='input_item clearfix']/input[@type='password']")).clear();
			driver.findElement(By.xpath(baseBy+"/div[@class='input_item clearfix']/input[@type='password']")).sendKeys(new String(Base64.decodeBase64(PropertiesUtils.getConfig("lagou.password"))));
			Thread.sleep(5000);
			driver.findElement(By.xpath(baseBy+"/div[@class='input_item btn_group clearfix']/input[@type='submit']")).click();
			Thread.sleep(5000);
			Set<Cookie> cookies = driver.manage().getCookies();
			for(Cookie c : cookies)
				System.out.println(c.getName()+"---"+c.getValue());
			//driver.close();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

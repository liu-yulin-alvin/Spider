package com.hbctcf.news.international.downloader;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TaobaoAction implements SeleniumAction {

	@Override
	public void execute(WebDriver driver) {
		 WebDriverWait wait = new WebDriverWait(driver, 10);
         // 商品页，避免加载过多无用图片信息。
         if (driver.getCurrentUrl().startsWith("https://item.taobao.com/")|| driver.getCurrentUrl().startsWith("https://detail.tmall.com")) {
             wait.until(ExpectedConditions.presenceOfElementLocated(By
                     .cssSelector("#J_PromoPriceNum")));
             return;
         }
         // 店铺首页，点击所有分类
         if ((!(driver.getCurrentUrl().startsWith("https://item.taobao.com")))
                 && !(driver.getCurrentUrl().startsWith("https://detail.tmall.com")) && (!driver.getCurrentUrl().contains("search"))) {
             WebElement allcate = driver.findElement(By
                     .cssSelector(".all-cats-trigger a"));
             Actions action = new Actions(driver);
             action.click(allcate).perform();
             try {
                 Thread.sleep(3000);
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
         }
         String url = driver.getCurrentUrl();
         // 列表页，加载所有
         WindowUtil.loadAll(driver);
         url = driver.getCurrentUrl();
         try {
             Thread.sleep(3000);
//             WindowUtil.taskScreenShot(driver, new File("d:\\data\\tb\\" + UUID.randomUUID().toString() + ".png"));
             // wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".pagination .page-info")));
         } catch (InterruptedException e) {
             e.printStackTrace();
         }

	}

}

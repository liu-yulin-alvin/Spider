package com.hbctcf.news.international.processor;

import com.hbctcf.news.international.utils.Constants;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

public class InternationalNewsProcessor implements PageProcessor {
	
	// 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
    private Site site = Site.me().setRetryTimes(3).setSleepTime(1000).setTimeOut(10000);

    // process是定制爬虫逻辑的核心接口，在这里编写抽取逻辑
	@Override
	public void process(Page page) {
		if(Constants.INTERNATIONAL_TARGET_URL.equals(page.getUrl().toString())){
			page.addTargetRequests(page.getHtml().xpath("//div[@class='blk12']/div[@class=blk121]/a").links().all());
			page.addTargetRequests(page.getHtml().xpath("//div[@class='blk12']/div[@class=blk122]/a").links().all());
			page.setSkip(true);
		} else {
			page.putField("title", page.getHtml().xpath("//h1[@id='artibodyTitle']/text()").toString());
			page.putField("content", page.getHtml().xpath("//div[@id='artibody']/tidyText()").toString());
			if(page.getResultItems().get("title") == null || page.getResultItems().get("content") == null){
				page.setSkip(true);
			}
		}
	}

	@Override
	public Site getSite() {
		return site;
	}

}

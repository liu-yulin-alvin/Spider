package com.hbctcf.news.international.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.hbctcf.news.international.dto.Lagou;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

public class LagouProcessor implements PageProcessor {

	private Site site = Site.me().setRetryTimes(3).setSleepTime(0).setTimeOut(3000);
	
	@Override
	public void process(Page page) {
		System.out.println(page.getHtml());
		int pageSize = page.getHtml().xpath("//div[@id='container']/div[@class='content_l']/div[@class='recommend_list']/ul[@class='rec_pos']/li[@class='rec_pos_item clearfix']").all().size();
		List<Lagou> lagouList = new ArrayList<>();
		for(int i=1;i<=pageSize;i++){
			String position = page.getHtml().xpath("//div[@id='container']/div[@class='content_l']/div[@class='recommend_list']/ul[@class='rec_pos']/li[@class='rec_pos_item clearfix']["+i+"]/div[@class='rec_pos_l']/div[@class='row']/a[@class='position_link']/tidyText()").get();
			String company =  page.getHtml().xpath("//div[@id='container']/div[@class='content_l']/div[@class='recommend_list']/ul[@class='rec_pos']/li[@class='rec_pos_item clearfix']["+i+"]/div[@class='rec_pos_r']/div[@class='coLeft']/div[@class='row']/a[@class='coName wordCut']/tidyText()").get();
			String salary = page.getHtml().xpath("//div[@id='container']/div[@class='content_l']/div[@class='recommend_list']/ul[@class='rec_pos']/li[@class='rec_pos_item clearfix']["+i+"]/div[@class='rec_pos_l']/div[@class='row']/span[@class='salary']/text()").get();
			String briefIntroduction = page.getHtml().xpath("//div[@id='container']/div[@class='content_l']/div[@class='recommend_list']/ul[@class='rec_pos']/li[@class='rec_pos_item clearfix']["+i+"]/div[@class='rec_pos_r']/div[@class='coLeft']/div[@class='row']/span[@class='coDetails c5 wordCut']/text()").get();
			Lagou lagou = new Lagou(position, company, salary, briefIntroduction);
			lagouList.add(lagou);
		}
		page.putField("lagouList", lagouList);
		
		int size = page.getHtml().xpath("//div[@id='container']/div[@class='content_l']/div[@class='Pagination myself']/a").all().size();
		List<Request> requestList = page.getTargetRequests();
		System.out.println(requestList);
		//if(requestList.size()<=1){
			for(int i=1;i<=size;i++){
				String pageNo = page.getHtml().xpath("//div[@id='container']/div[@class='content_l']/div[@class='Pagination myself']/a["+i+"]/text()").get();
				if(StringUtils.isNumeric(pageNo))
				page.addTargetRequest("https://www.lagou.com/jobs/mList.html?pageNo="+pageNo);
			}
				
		//}
	}

	@Override
	public Site getSite() {
		return site;
	}

}

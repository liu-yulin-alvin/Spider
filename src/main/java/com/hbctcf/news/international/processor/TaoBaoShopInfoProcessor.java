package com.hbctcf.news.international.processor;

import java.util.concurrent.atomic.AtomicBoolean;

import com.hbctcf.news.international.downloader.WebDriverPool;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

public class TaoBaoShopInfoProcessor implements PageProcessor {

	private Site site = Site
            .me()
            .setCharset("UTF-8")
            .setCycleRetryTimes(3)
            .setSleepTime(3 * 1000)
            .addHeader("Connection", "keep-alive")
            .addHeader("Cache-Control", "max-age=0")
            .addHeader("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
	
	 private AtomicBoolean isPageAdd = new AtomicBoolean(false);
	    private static AtomicBoolean running = new AtomicBoolean(false);
	    private WebDriverPool pool=new WebDriverPool();
	
	@Override
	public void process(Page page) {
		// TODO Auto-generated method stub

	}

	@Override
	public Site getSite() {
		// TODO Auto-generated method stub
		return site;
	}

}

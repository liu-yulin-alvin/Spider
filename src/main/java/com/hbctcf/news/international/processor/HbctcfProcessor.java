package com.hbctcf.news.international.processor;

import com.hbctcf.news.international.utils.Constants;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

public class HbctcfProcessor implements PageProcessor {
	
	private static final String urlDetailRegex = "https://www\\.hbctcf\\.com/hashfinance/detail/\\d+";
	
	// 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
    private Site site = Site.me().setRetryTimes(3).setTimeOut(10000);
    
	@Override
	public void process(Page page) {
		//System.out.println(page.getHtml().toString());
		if(Constants.HBCTCF_TARGET_URL.equals(page.getUrl().toString())){
			page.addTargetRequests(page.getHtml().xpath("//table[@id='regularTable']/tbody/tr/td/a[@class='tda']").links().all());
			page.setSkip(true);
		} else if(page.getUrl().regex(urlDetailRegex).match()) {
			page.putField("borrowTitle", page.getHtml().xpath("//div[@id='licaiDetails']/div[@class='row licaiDetailsTop']/ol[@class='breadcrumb']/li[3]/a/text()").toString());
			page.putField("borrowSum", page.getHtml().xpath("//div[@id='licaiDetails']/div[@class='row licaiDetailsMain']/div[@class='col-md-8']/ul[@class='list-inline ul1']/li[1]/p[@class='p2']/text()").toString());
			page.putField("borrowRate", page.getHtml().xpath("//div[@id='licaiDetails']/div[@class='row licaiDetailsMain']/div[@class='col-md-8']/ul[@class='list-inline ul1']/li[2]/p[@class='p3']/text()").toString());
			page.putField("borrowTerm", page.getHtml().xpath("//div[@id='licaiDetails']/div[@class='row licaiDetailsMain']/div[@class='col-md-8']/ul[@class='list-inline ul1']/li[3]/p[@class='p2']/text()").toString());
			page.putField("borrowMinInvestMoney", page.getHtml().xpath("//div[@id='licaiDetails']/div[@class='row licaiDetailsMain']/div[@class='col-md-8']/ul[@class='list-inline ul1']/li[4]/p[@class='p2']/text()").toString());
		} else {
			page.setSkip(true);
		}
	}

	@Override
	public Site getSite() {
		return site;
	}

}

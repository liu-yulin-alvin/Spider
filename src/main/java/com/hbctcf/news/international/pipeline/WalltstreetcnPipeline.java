package com.hbctcf.news.international.pipeline;

import com.hbctcf.news.international.dto.Wallstreetcn;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

public class WalltstreetcnPipeline implements Pipeline {

	@Override
	public void process(ResultItems resultItems, Task task) {
		String title = resultItems.get("title");
		String content = resultItems.get("content");
		
		Wallstreetcn walltstreetcn = new Wallstreetcn();
		walltstreetcn.setTitle(title);
		walltstreetcn.setContent(content);
		System.out.println("---->"+walltstreetcn);

	}

}

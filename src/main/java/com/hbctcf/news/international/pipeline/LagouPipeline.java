package com.hbctcf.news.international.pipeline;

import java.util.List;

import com.hbctcf.news.international.dto.Lagou;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

public class LagouPipeline implements Pipeline {

	@Override
	public void process(ResultItems resultItems, Task task) {
		List<Lagou> lagouList = resultItems.get("lagouList");
		for(Lagou lagou : lagouList){
			System.out.println(lagou);
		}
		System.err.println("=========================");
	}

}

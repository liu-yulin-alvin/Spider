package com.hbctcf.news.international.pipeline;

import com.hbctcf.news.international.dto.InternationalNews;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

public class InternationalNewsPipeline implements Pipeline {

	@Override
	public void process(ResultItems resultItems, Task task) {
		InternationalNews internationalNews = new InternationalNews();
		String title = resultItems.get("title");
		String content = resultItems.get("content");
		internationalNews.setTitle(title);
		internationalNews.setContent(content);
		System.out.println("---->"+internationalNews);
	}

}

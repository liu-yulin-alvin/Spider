package com.hbctcf.news.international.pipeline;

import com.alibaba.fastjson.JSON;
import com.hbctcf.news.international.dto.WangyiMusic;

import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.PageModelPipeline;

public class WangyiMusicPipeline implements PageModelPipeline<WangyiMusic> {

	@Override
	public void process(WangyiMusic wangyiMusic, Task task) {
		
		System.out.println(JSON.toJSONString(wangyiMusic));
	}

}

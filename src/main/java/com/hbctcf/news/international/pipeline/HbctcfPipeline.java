package com.hbctcf.news.international.pipeline;

import com.hbctcf.news.international.dto.Hbctcf;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

public class HbctcfPipeline implements Pipeline {

	@Override
	public void process(ResultItems resultItems, Task task) {
		Hbctcf hbctcf = new Hbctcf();
		hbctcf.setBorrowTitle((String)resultItems.get("borrowTitle"));
		hbctcf.setBorrowSum((String)resultItems.get("borrowSum"));
		hbctcf.setBorrowRate((String)resultItems.get("borrowRate"));
		hbctcf.setBorrowTerm((String)resultItems.get("borrowTerm"));
		hbctcf.setBorrowMinInvestMoney((String)resultItems.get("borrowMinInvestMoney"));
		System.out.println("--->"+hbctcf);
	}

}

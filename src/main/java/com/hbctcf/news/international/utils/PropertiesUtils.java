package com.hbctcf.news.international.utils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertiesUtils {

	public static Properties config = new Properties();
	
	static {
		try {
			config.load(new FileReader("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("���������ļ�����");
		}
	}
	
	public static String getConfig(String key){
		if (config.containsKey(key)) {
			return config.getProperty(key);
		} else {
			return null;
		}
	}
}

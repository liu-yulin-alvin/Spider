package com.hbctcf.news.international.utils;

public class Constants {

	/** 新浪国际新闻target */
	public static final String INTERNATIONAL_TARGET_URL = PropertiesUtils.getConfig("sina.international.targeturl");
	/** 楚天财富 target */
	public static final String HBCTCF_TARGET_URL = PropertiesUtils.getConfig("hbctcf.borrow.targeturl");
	/** 华尔街见闻target */
	public static final String WALLSTREET_TARGET_URL = PropertiesUtils.getConfig("wallstreet.targeturl");

	public static final String SECRET_KEY = "sdw234ekw83hf882389rh2938r2";
}
